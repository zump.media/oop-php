<?php
class animal{
    public $name;
    public $legs = 4;
    public $cold_blooded = "No";

    public function __construct($string){
        $this->name = $string;
    }
}
//$sheep = new Animal("shaun");

////echo $sheep->name; // "shaun"
//echo $sheep->legs; // 4
//echo $sheep->cold_blooded; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>